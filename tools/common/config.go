package common

import (
	"code.google.com/p/gcfg"
	"fmt"
	"os/user"
)

// Config mirrors the format expected from the config file.
// gcfg package will load data from the config file into 
// one of these structs
type Config struct {
	Production struct {
		Email     string
		Password  string
		UploadUrl string
	}
	Development struct {
		Email     string
		Password  string
		UploadUrl string
	}
}

// GetServerFromConfig reads the file named and uses the configuration
// it finds there to fill a common.Server struct with data ready to pass
// to common.OneFile
//
// If isDev is true it uses the configuration in the file listed under "Development"
// otherwise it uses the configuration in the file listed under "Production"
//
// If it finds no file at the given configFilename it tries again prepending
// the user's home directory to the string. This is so you can pass ".nometic.gcfg"
// in as a parameter here and put the file in your home directory.
func GetServerFromConfig(configFilename string, isDev bool) (Server, error) {
	var cfg Config
	if err := gcfg.ReadFileInto(&cfg, configFilename); err != nil {
		user, uerr := user.Current()
		if uerr != nil {
			return Server{}, uerr
		}

		homeconfig := fmt.Sprintf("%s/%s", user.HomeDir, configFilename)
		if inerr := gcfg.ReadFileInto(&cfg, homeconfig); inerr != nil {
			return Server{}, inerr
		}
	}

	serverStruct := cfg.Production
	if isDev {
		serverStruct = cfg.Development
	}

	server := Server{
		serverStruct.Email,
		serverStruct.Password,
		serverStruct.UploadUrl,
	}

	return server, nil
}
