// Copyright 2013 Patrick Borgeest.
// Copyright 2011 The Go Authors. All rights reserved.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// THIS FILE IS A MODIFIED COPY OF
//  https://code.google.com/p/appengine-go/source/browse/example/moustachio/moustachio/http.go
//
// Modified by Patrick Borgeest
//

package resize

import (
	"bytes"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	"image/png"
	"io"
)

// This interface will presumably change so that decoding
// into image.Image happens here rather than in the caller
func ResizeImage(reader io.Reader, max int) ([]byte, error) {

	var buf bytes.Buffer
	_, err := io.Copy(&buf, reader)
	i, _, err := image.Decode(&buf)
	if err != nil {
		return buf.Bytes(), err
	}

	// Resize if too large
	if b := i.Bounds(); b.Dx() > max || b.Dy() > max {
		// If it's gigantic, it's more efficient to downsample first
		// and then resize; resizing will smooth out the roughness.
		if b.Dx() > 2*max || b.Dy() > 2*max {
			w, h := max, max
			if b.Dx() > b.Dy() {
				h = b.Dy() * h / b.Dx()
			} else {
				w = b.Dx() * w / b.Dy()
			}
			i = Resample(i, i.Bounds(), w, h)
			b = i.Bounds()
		}
		w, h := max, max
		if b.Dx() > b.Dy() {
			h = b.Dy() * h / b.Dx()
		} else {
			w = b.Dx() * w / b.Dy()
		}
		i = Resize(i, i.Bounds(), w, h)
	}

	buf.Reset()
	err = png.Encode(&buf, i)
	return buf.Bytes(), err
}
