/*
Copyright 2013 Patrick Borgeest
Copyright 2011 Google Inc. All rights reserved. (isErrFieldMismatch() function from stdlib)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package nometicland

import (
	"appengine"
	"appengine/blobstore"
	"appengine/datastore"
	"comma"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	mediaObjectKind = "MediaObject"
	userInfoKind    = "UserInfo"
	documentKind    = "Document"
)

// GetOrInsert takes the email of the user object that Google provides
// and either gets the matching UserInfo struct from the database
// or creates one if there is none, stores it, and returns that
func GetOrInsert(c appengine.Context, authEmail string) (*datastore.Key, UserInfo, error) {
	q := datastore.NewQuery("UserInfo").
		Filter("User =", authEmail).
		Limit(1)
	t := q.Run(c)
	var ui UserInfo
	key, err := t.Next(&ui)
	if err == datastore.Done {
		// insert new UserInfo here
		ui = UserInfo{
			User:         authEmail,
			MediaObjects: 0,
		}
		key, err := datastore.Put(c, datastore.NewIncompleteKey(c, userInfoKind, nil), &ui)
		return key, ui, err
	}
	return key, ui, err
}

// FetchDocumentById returns the Document struct from the datastore that
// matches the given ID
func FetchDocumentById(c appengine.Context, intId int64, userKey *datastore.Key) (*Document, error) {
	key := datastore.NewKey(c, documentKind, "", intId, userKey)
	var obj Document
	err := datastore.Get(c, key, &obj)
	obj.StoreKey(key)
	if err != nil && !isErrFieldMismatch(err) {
		return &obj, err
	}
	return &obj, nil
}

// FetchMediaById returns the MediaObject struct from the datastore that
// matches the given ID
func FetchMediaById(c appengine.Context, intId int64, userKey *datastore.Key) (*MediaObject, error) {
	key := datastore.NewKey(c, mediaObjectKind, "", intId, userKey)
	var obj MediaObject
	err := datastore.Get(c, key, &obj)
	obj.StoreKey(key)
	if err != nil && !isErrFieldMismatch(err) {
		return &obj, err
	}
	return &obj, nil
}

// FetchMediaForUser returns all MediaObject structs from the datastore 
// that are owned by the user with the given userKey that are NOT at present
// attached to a document
func FetchMediaForUser(c appengine.Context, userKey *datastore.Key, limit int) ([]MediaObject, error) {
	q := datastore.NewQuery("MediaObject").
		Filter("Owner =", userKey).
		Filter("LacksDocument =", true).
		Order("Creation").
		Limit(limit)
	var objs []MediaObject
	keys, err := q.GetAll(c, &objs)
	if err != nil && !isErrFieldMismatch(err) {
		return objs, err
	}
	for i, key := range keys {
		objs[i].StoreKey(key)
	}
	return objs, nil
}

// FetchDocumentsForUser returns all Document structs from the datastore 
// that are owned by the user with the given userKey 
func FetchDocumentsForUser(c appengine.Context, userKey *datastore.Key, limit int) ([]*Document, error) {
	q := datastore.NewQuery("Document").
		Filter("Owner =", userKey).
		Order("-Creation").
		Limit(limit)
	var objs []*Document
	keys, err := q.GetAll(c, &objs)
	if err != nil && !isErrFieldMismatch(err) {
		return objs, err
	}
	for i, key := range keys {
		objs[i].StoreKey(key)
	}
	return objs, nil
}

func FetchTaggedDocumentsForUser(c appengine.Context, tags comma.SeparatedString, userKey *datastore.Key) ([]*Document, error) {
	output := make([]*Document, 0)
	q := datastore.NewQuery("Document").
		Filter("Owner =", userKey).
		Order("Creation").
		Filter("NoTags =", false)
	for _, tag := range tags {
		q = q.Filter("LowercaseTags =", strings.ToLower(tag))
	}

	keys, err := q.GetAll(c, &output)
	if err != nil && !isErrFieldMismatch(err) {
		return output, err
	}
	for i, key := range keys {
		output[i].StoreKey(key)
	}
	return output, nil
}

// FetchUntaggedDocumentsForUser returns all Document structs from the datastore 
// that are owned by the user with the given userKey and have no tags set
func FetchUntaggedDocumentsForUser(c appengine.Context, userKey *datastore.Key, limit int) ([]*Document, error) {
	q := datastore.NewQuery("Document").
		Filter("Owner =", userKey).
		Filter("NoTags =", true).
		Order("Creation").
		Limit(limit)
	var objs []*Document
	keys, err := q.GetAll(c, &objs)
	if err != nil && !isErrFieldMismatch(err) {
		return objs, err
	}
	for i, key := range keys {
		objs[i].StoreKey(key)
	}
	return objs, nil
}

// FetchUpcomingDueDocumentsForUser returns all Document structs from the datastore 
// that are owned by the user with the given userKey with approaching due dates
func FetchUpcomingDueDocumentsForUser(c appengine.Context, userKey *datastore.Key) ([]*Document, error) {
	var zerotime time.Time
	q := datastore.NewQuery("Document").
		Filter("Owner =", userKey).
		Filter("DueDate >", zerotime). // If it has a due date set
		Order("DueDate").
		Limit(30)
	var objs []*Document
	keys, err := q.GetAll(c, &objs)
	if err != nil && !isErrFieldMismatch(err) {
		return objs, err
	}
	for i, key := range keys {
		objs[i].StoreKey(key)
	}
	return objs, nil
}

// FetchMediaObjectsByIds returns all MediaObject structs from the datastore
// that have the given keys
func FetchMediaObjectsByIds(c appengine.Context, keys []*datastore.Key) ([]MediaObject, error) {
	objs := make([]MediaObject, len(keys))
	err := datastore.GetMulti(c, keys, objs)
	if err != nil && isErrMultiError(err) {
		me := convertErrorToMultiError(err)
		for _, thiserr := range me {
			// if any of the errors in the multierror are not ErrFieldMismatch then return them all
			if thiserr != nil && !isErrFieldMismatch(thiserr) {
				return objs, err
			}
		}
	} else {
		if err != nil && !isErrFieldMismatch(err) {
			return objs, err
		}
	}
	for i, key := range keys {
		objs[i].StoreKey(key)
	}
	return objs, nil
}

// UpdateDocument writes to the datastore a given document
// assuming the document has previously been stored and has
// its IntID stored
func UpdateDocument(c appengine.Context, doc *Document, userKey *datastore.Key) error {
	key := datastore.NewKey(c, documentKind, "", doc.IntID, userKey)
	_, err := datastore.Put(c, key, doc)
	return err
}

// IncrementUsersMediaObjects adds one to the MediaObjects field of the
// UserInfo struct in the database that matches the given userKey
// Calling functions must run this inside a transaction
func IncrementUsersMediaObjects(c appengine.Context, userKey *datastore.Key) error {
	userInfo := new(UserInfo)
	if err := datastore.Get(c, userKey, userInfo); err != nil && !isErrFieldMismatch(err) {
		return err
	}
	userInfo.MediaObjects = userInfo.MediaObjects + 1
	_, incrErr := datastore.Put(c, userKey, userInfo)
	return incrErr
}

// StoreMedia creates a new MediaObject struct that represents the given uploadFile
// and stores it in the datastore.
func StoreMedia(c appengine.Context, userKey *datastore.Key, uploadFile *blobstore.BlobInfo, formVals url.Values) error {
	contentType := DetectContentType(c, uploadFile)
	object := NewMediaObject(userKey, uploadFile, contentType)

	return datastore.RunInTransaction(c, func(c appengine.Context) error {
		_, puterr := datastore.Put(c, datastore.NewIncompleteKey(c, mediaObjectKind, userKey), object)
		if puterr != nil {
			return puterr
		}

		return IncrementUsersMediaObjects(c, userKey)
	}, nil)
}

// SaveDocumentAndUpdateMediaObjects creates a new Document struct that represents
// the given MediaObject structs and stores it in the datastore, updates each of 
// these MediaObject in the datastore with references back to the new Document struct
// and returns the key to the new document entity
func SaveDocumentAndUpdateMediaObjects(c appengine.Context, userKey *datastore.Key, newDoc *Document, scanKeys []*datastore.Key) (*datastore.Key, error) {
	docKey := new(datastore.Key)
	err := datastore.RunInTransaction(c, func(c appengine.Context) error {
		newdocKey, docputErr := datastore.Put(c, datastore.NewIncompleteKey(c, documentKind, userKey), newDoc)
		if docputErr != nil {
			return docputErr
		}
		docKey = newdocKey

		for _, moKey := range scanKeys {
			mediaObject := new(MediaObject)
			if err := datastore.Get(c, moKey, mediaObject); err != nil && !isErrFieldMismatch(err) {
				return err
			}
			mediaObject.LacksDocument = false
			mediaObject.Document = docKey
			if _, err := datastore.Put(c, moKey, mediaObject); err != nil {
				return err
			}
		}

		return nil
	}, nil)
	return docKey, err
}

// GetKeyFromId takes a kind and the string that represents the id of the kind
// and returns the appropriate datastore.Key
func GetKeyFromId(c appengine.Context, kind string, scanId string, userKey *datastore.Key) (*datastore.Key, error) {
	intId, err := strconv.ParseInt(scanId, 10, 64)
	if err != nil {
		return nil, err
	}
	key := datastore.NewKey(c, kind, "", intId, userKey)
	return key, nil
}

// GetKeysFromIds takes a kind and a slice of strings that represents the ids of the kind
// and returns a slice of the appropriate datastore.Keys
func GetKeysFromIds(c appengine.Context, kind string, scanIds []string, userKey *datastore.Key) ([]*datastore.Key, error) {
	keys := make([]*datastore.Key, len(scanIds))
	for i := 0; i < len(scanIds); i++ {
		key, err := GetKeyFromId(c, kind, scanIds[i], userKey)
		if err != nil {
			return keys, err
		}
		keys[i] = key
	}
	return keys, nil
}

// LookupAndAuthenticateUser return the key to the UserInfo that matches the email
// address and the password that are passed in. If no user matches then nil is
// returned.
func LookupAndAuthenticateUser(c appengine.Context, claimedEmail, password string) *datastore.Key {
	q := datastore.NewQuery("UserInfo").
		Filter("User =", claimedEmail).
		Filter("UploadPassword =", password).
		Limit(1)

	t := q.Run(c)
	ui := new(UserInfo)
	if key, err := t.Next(ui); err == nil {
		return key
	}
	return nil
}

// DetectContentType returns the mimetype of the data stored at uploadFile
// or "application/octet-stream" if it cannot 
func DetectContentType(c appengine.Context, uploadFile *blobstore.BlobInfo) string {
	const defaultMIME = "application/octet-stream"
	if uploadFile.ContentType != defaultMIME {
		return uploadFile.ContentType
	}

	// read data [512]bytes of blobinfo
	reader := blobstore.NewReader(c, uploadFile.BlobKey)
	var limitedReader = io.LimitedReader{
		R: reader,
		N: 512,
	}
	bytes := make([]byte, 512)
	if _, err := limitedReader.Read(bytes); err != nil {
		return defaultMIME
	}

	return http.DetectContentType(bytes)
}

// BreakAndDeleteDoc deletes the given document struct and marks all of its
// associated MediaObjects as not being part of a document
func BreakAndDeleteDoc(c appengine.Context, document *Document, userKey *datastore.Key) error {
	return datastore.RunInTransaction(c, func(c appengine.Context) error {
		for _, moKey := range document.Pages {
			mediaObject := new(MediaObject)
			if err := datastore.Get(c, moKey, mediaObject); err != nil {
				return err
			}
			mediaObject.LacksDocument = true
			mediaObject.Document = nil
			if _, err := datastore.Put(c, moKey, mediaObject); err != nil {
				return err
			}
		}
		return datastore.Delete(c, datastore.NewKey(c, documentKind, "", document.IntID, userKey))
	}, nil)
}

// DeleteDocAndImages deletes the given document struct and all of its associated MediaObjects
func DeleteDocAndImages(c appengine.Context, document *Document, userKey *datastore.Key) error {
	return datastore.RunInTransaction(c, func(c appengine.Context) error {
		pagesCount := len(document.Pages)
		for _, moKey := range document.Pages {
			mediaObject := new(MediaObject)
			if err := datastore.Get(c, moKey, mediaObject); err != nil {
				return err
			}
			if err := blobstore.Delete(c, mediaObject.Blob); err != nil {
				return err
			}
			if err := datastore.Delete(c, moKey); err != nil {
				return err
			}
		}

		userInfo := new(UserInfo)
		if err := datastore.Get(c, userKey, userInfo); err != nil {
			return err
		}
		userInfo.MediaObjects = userInfo.MediaObjects - int64(pagesCount)
		if _, err := datastore.Put(c, userKey, userInfo); err != nil {
			return err
		}

		return datastore.Delete(c, datastore.NewKey(c, documentKind, "", document.IntID, userKey))
	}, nil)
}

// FetchAllTags returns a slice of strings where each string is a comma-separated
// list of tags. Each element of the slice is the list of tags for a document. All
// documents for the given user are returned, that is, the length of the slice is
// the number of documents currently loaded by the user
func FetchAllTags(c appengine.Context, userKey *datastore.Key) ([]string, error) {
	allTags := make([]string, 0)
	fetchedDocs, err := FetchTaggedDocumentsForUser(c, make([]string, 0), userKey)
	if err != nil {
		return allTags, err
	}

	for _, thisDoc := range fetchedDocs {
		allTags = append(allTags, thisDoc.Tags.String())
	}

	return allTags, nil
}

// From http://code.google.com/p/appengine-go/source/browse/appengine/blobstore/blobstore.go
func isErrFieldMismatch(err error) bool {
	_, ok := err.(*datastore.ErrFieldMismatch)
	return ok
}

func convertErrorToMultiError(err error) appengine.MultiError {
	me, _ := err.(appengine.MultiError)
	return me
}
func isErrMultiError(err error) bool {
	_, ok := err.(appengine.MultiError)
	return ok
}
