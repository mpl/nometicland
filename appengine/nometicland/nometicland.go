/*
Copyright 2013 Patrick Borgeest
Copyright 2009 Brad Fitzpatrick <brad@danga.com> (nometicland is a derivative work of scanningcabinet)
Copyright 2009 Google Inc. (sample app that scanningcabinet is based on)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/* All code was written by Patrick Borgeest but many lines in 
   this file are direct translations into go of Fitzpatrick's
   python code
*/

package nometicland

import (
	"comma"
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"net/url"
	"resize"
	"strconv"
	"strings"
	"time"

	"appengine"
	"appengine/blobstore"
	"appengine/datastore"
	"appengine/user"
)

var (
	uploadGetTemplate = template.Must(template.ParseFiles("templates/uploadget.tmpl"))
	rootTemplate      = template.Must(template.ParseFiles("templates/root.tmpl"))
	docTemplate       = template.Must(template.ParseFiles("templates/doc.tmpl"))
)

func init() {
	http.HandleFunc("/", handleRoot)
	http.HandleFunc("/serve/", handleServe)
	http.HandleFunc("/upload", handleUpload)
	http.HandleFunc("/resource/", handleResource)
	http.HandleFunc("/makedoc", handleMakedoc)
	http.HandleFunc("/changedoc", handleChangedoc)
	http.HandleFunc("/uploadurl", handleUploadUrl)
	http.HandleFunc("/doc/", handleDoc)
	http.HandleFunc("/robots.txt", handleRobots)
}

func serveError(c appengine.Context, w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Header().Set("Content-Type", "text/plain")
	io.WriteString(w, "Internal Server Error")
	c.Errorf("%v", err)
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	var login_url string

	c := appengine.NewContext(r)

	if u := user.Current(c); u == nil {
		login_url, _ = user.LoginURL(c, "/")
		http.Redirect(w, r, login_url, http.StatusFound)
		return
	} else {
		login_url, _ = user.LogoutURL(c, "/")
	}
	var limit int
	limit = 50
	if limitparam := r.FormValue("limit"); limitparam != "" {
		newlimit, err := strconv.Atoi(limitparam)
		if err == nil {
			limit = newlimit
		}
	}
	userKey, userInfo := GetUserInfo(c)

	// fetch media objects
	mediaObjects, err := FetchMediaForUser(c, userKey, limit)
	if err != nil {
		serveError(c, w, err)
	}
	mediaObjectViewModels := MakeMediaObjectViewModels(mediaObjects)

	// fetch all documents
	// Not yet implemented the tag search
	tagsparam := r.FormValue("tags")
	didSearch := (tagsparam != "")
	var docs []*Document
	if didSearch {
		tags := comma.NewSeparatedString(r.FormValue("tags"))
		fetchedDocs, err := FetchTaggedDocumentsForUser(c, tags, userKey)
		if err != nil {
			serveError(c, w, err)
			return
		}
		docs = fetchedDocs
	} else {
		fetchedDocs, err := FetchDocumentsForUser(c, userKey, limit)
		if err != nil {
			serveError(c, w, err)
			return
		}
		docs = fetchedDocs
	}

	documentViewModels := MakeDocumentViewModels(docs)

	// fetch upcoming documents
	upcomingdocs, err := FetchUpcomingDueDocumentsForUser(c, userKey)
	if err != nil {
		serveError(c, w, err)
		return
	}
	upcomingDocumentViewModels := MakeDocumentViewModels(upcomingdocs)

	// fetch untagged documents
	untaggeddocs, err := FetchUntaggedDocumentsForUser(c, userKey, limit)
	if err != nil {
		serveError(c, w, err)
		return
	}
	untaggedDocumentViewModels := MakeDocumentViewModels(untaggeddocs)

	topMessage := ""
	if saved_doc := r.FormValue("saved_doc"); saved_doc != "" {
		topMessage = fmt.Sprintf("Saved <a href='/doc/%s'>doc %s</a>", saved_doc, saved_doc)
	}
	errorMessage := r.FormValue("error_message")

	noDocs := (!(len(documentViewModels) > 0))
	allTags := make([]string, 0)
	if noDocs {
		allTags, _ = FetchAllTags(c, userKey)
	}

	d := struct {
		UserInfo                UserInfo
		LoginUrl                string
		Media                   []MediaObjectVM
		Docs                    []DocumentVM
		NoDocs                  bool
		UntaggedDocs            []DocumentVM
		UpcomingDocs            []DocumentVM
		TopMessage              template.HTML
		ErrorMessage            string
		HasMediaAndDidNotSearch bool
		AllTags                 []string
		AllTagCount             int
	}{
		*userInfo,
		login_url,
		mediaObjectViewModels,
		documentViewModels,
		noDocs,
		untaggedDocumentViewModels,
		upcomingDocumentViewModels,
		template.HTML(topMessage),
		errorMessage,
		(len(mediaObjectViewModels) > 0 && !didSearch),
		allTags,
		len(allTags),
	}
	if err := rootTemplate.Execute(w, d); err != nil {
		serveError(c, w, err)
	}
}

func handleServe(w http.ResponseWriter, r *http.Request) {
	blobstore.Send(w, appengine.BlobKey(r.FormValue("blobKey")))
}

func handleUploadGet(c appengine.Context, w http.ResponseWriter, r *http.Request) error {
	uploadURL, err := blobstore.UploadURL(c, "/upload", nil)
	if err != nil {
		return err
	}
	w.Header().Set("Content-Type", "text/html")
	d := struct{ UploadUrl *url.URL }{uploadURL}
	err = uploadGetTemplate.Execute(w, d)
	if err != nil {
		return err
	}
	return nil
}

func handleChangedoc(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	if r.Method != "POST" {
		serveError(c, w, errors.New("Not POST"))
		return
	}
	userKey, _ := GetUserInfo(c)
	if userKey == nil {
		http.Redirect(w, r, "/?error_message=log-in+required", http.StatusFound)
	}

	docId, _ := strconv.ParseInt(r.FormValue("docid"), 10, 64)

	document, err := FetchDocumentById(c, docId, userKey)
	if err != nil {
		serveError(c, w, err)
		return
	}
	if document == nil {
		serveError(c, w, errors.New(fmt.Sprintf("Docid %d not found.", docId)))
		return
	}

	mode := r.FormValue("mode")
	if mode == "break" {
		if err := BreakAndDeleteDoc(c, document, userKey); err != nil {
			serveError(c, w, err)
			return
		}
		fmt.Fprintf(w, "<html><body>[&lt;&lt; <a href='/'>Back</a>] Docid %d deleted and images broken out as un-annotated.</body></html>", docId)
		return
	}
	if mode == "delete" {
		if err := DeleteDocAndImages(c, document, userKey); err != nil {
			serveError(c, w, err)
			return
		}
		fmt.Fprintf(w, "<html><body>[&lt;&lt; <a href='/'>Back</a>] Docid %d and its images deleted.</body></html>", docId)
		return
	}

	document.PhysicalLocation = r.FormValue("physical_location")
	document.Title = r.FormValue("title")

	document.SetTags(comma.NewSeparatedString(r.FormValue("tags")))

	if t, err := time.Parse(DateformatYyyyMmDd, r.FormValue("date")); err == nil {
		document.DocDate = t
		document.NoDate = false
	} else {
		var zerotime time.Time
		document.DocDate = zerotime
		document.NoDate = true
	}

	if duedate, err := time.Parse(DateformatYyyyMmDd, r.FormValue("due_date")); err == nil {
		document.DueDate = duedate
	} else {
		var zerotime time.Time
		document.DueDate = zerotime
	}

	if err := UpdateDocument(c, document, userKey); err != nil {
		serveError(c, w, err)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/?saved_doc=%d", docId), http.StatusFound)
}

func handleUpload(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	userKey, _ := GetUserInfo(c)
	if r.Method != "POST" {
		serveError(c, w, errors.New("Not POST"))
		/*
			// During development - before I had the commandline client written,
			// I was using this form to upload scans into the webapp
			if err := handleUploadGet(c, w, r); err != nil {
				serveError(c, w, err)
			}
		*/
		return
	}
	blobs, formVals, err := blobstore.ParseUpload(r)
	if err != nil {
		serveError(c, w, err)
		return
	}

	file := blobs["file"]
	if len(file) == 0 {
		c.Errorf("no file uploaded")
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	if err := r.ParseForm(); err != nil {
		blobstore.Delete(c, file[0].BlobKey)
		serveError(c, w, err)
		return
	}

	if userKey == nil {
		claimedEmail := formVals.Get("user_email")
		if userKey = LookupAndAuthenticateUser(c, claimedEmail, formVals.Get("password")); userKey == nil {
			c.Errorf("Could not authenticate %s [%s]", claimedEmail, formVals.Get("password"))
			blobstore.Delete(c, file[0].BlobKey)
			http.Redirect(w, r, "/?error_message=No+user+or+correct+password", http.StatusFound)
		}
	}

	transerr := StoreMedia(c, userKey, file[0], formVals)
	if transerr != nil {
		blobstore.Delete(c, file[0].BlobKey)
		serveError(c, w, err)
		return
	}

	//http.Redirect(w, r, "/serve/?blobKey="+string(file[0].BlobKey), http.StatusFound)
	http.Redirect(w, r, "/", http.StatusFound)
}

func handleMakedoc(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	if r.Method != "POST" {
		serveError(c, w, errors.New("Not POST"))
		return
	}
	userKey, _ := GetUserInfo(c)
	if userKey == nil {
		http.Redirect(w, r, "/?error_message=log-in+required", http.StatusFound)
	}

	// gather the media_ids from the form into scanKeys
	if r.Form == nil {
		r.ParseMultipartForm(1)
	}
	scan_ids := r.Form["media_id"]
	scanKeys, err := GetKeysFromIds(c, mediaObjectKind, scan_ids, userKey)
	if err != nil {
		serveError(c, w, err)
		return
	}

	newDoc := NewDocument(userKey, scanKeys)

	newDocKey, err := SaveDocumentAndUpdateMediaObjects(c, userKey, newDoc, scanKeys)
	if err != nil {
		serveError(c, w, err)
		return
	}

	newDoc.StoreKey(newDocKey)

	http.Redirect(w, r, fmt.Sprintf("%s?size=1200", newDoc.DisplayUrl()), http.StatusFound)
}

func handleDoc(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	if r.Method != "GET" {
		serveError(c, w, errors.New("Not GET"))
		return
	}
	userKey, userInfo := GetUserInfo(c)
	if userKey == nil {
		http.Redirect(w, r, "/?error_message=log-in+required", http.StatusFound)
	}

	var login_url string
	if u := user.Current(c); u == nil {
		login_url, _ = user.LoginURL(c, "/")
		http.Redirect(w, r, login_url, http.StatusFound)
		return
	} else {
		login_url, _ = user.LogoutURL(c, "/")
	}
	docId, err := mungeDocUrl(r.URL.Path)
	if err != nil {
		serveError(c, w, err)
		return
	}

	document, err := FetchDocumentById(c, docId, userKey)
	if err != nil {
		serveError(c, w, err)
		return
	}
	if document == nil {
		http.Redirect(w, r, fmt.Sprintf("/?error_message=DocId+%d+not+found", docId), http.StatusFound)
	}

	pages, err := FetchMediaObjectsByIds(c, document.Pages)
	if err != nil {
		serveError(c, w, err)
		return
	}
	var size int
	size = 1200
	if sizeParam := r.FormValue("size"); sizeParam != "" {
		sizeint, err := strconv.Atoi(sizeParam)
		if err != nil {
			serveError(c, w, err)
			return
		}
		size = sizeint
	}
	show_single_list := size > 600

	d := struct {
		UserInfo       UserInfo
		LoginUrl       string
		Pages          []MediaObjectVM
		Doc            DocumentVM
		ShowSingleList bool
		Size           int
	}{
		*userInfo,
		login_url,
		MakeMediaObjectViewModels(pages),
		document.MakeViewModel(),
		show_single_list,
		size,
	}
	if err := docTemplate.Execute(w, d); err != nil {
		serveError(c, w, err)
	}
}

func mungeDocUrl(path string) (int64, error) {
	pathelements := strings.Split(path, "/")
	return strconv.ParseInt(pathelements[2], 10, 64)
}

func handleUploadUrl(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	claimedEmail := r.FormValue("user_email")
	if effectiveUserKey := LookupAndAuthenticateUser(c, claimedEmail, r.FormValue("password")); effectiveUserKey != nil {
		uploadURL, err := blobstore.UploadURL(c, "/upload", nil)
		if err != nil {
			serveError(c, w, err)
			return
		}
		w.Header().Set("Content-Type", "text/plain")
		fmt.Fprintf(w, "%s", uploadURL)
		return
	}

	w.WriteHeader(http.StatusForbidden)
}

func handleResource(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	userKey, _ := GetUserInfo(c)
	if userKey == nil {
		http.Redirect(w, r, "/?error_message=log-in+required", http.StatusFound)
	}

	mediaId, err := mungeResourceUrl(r.URL.Path)
	if err != nil {
		serveError(c, w, err)
		return
	}

	mediaObject, err := FetchMediaById(c, mediaId, userKey)
	if err != nil {
		serveError(c, w, err)
		return
	}
	if mediaObject == nil {
		http.Redirect(w, r, "/?error_message=Unidentified+object", http.StatusFound)
	}

	if statusNotFound := setResourceHeaders(w, r, mediaObject); statusNotFound {
		return
	}

	blobkey := mediaObject.Blob

	if resizeParam := r.FormValue("resize"); resizeParam != "" {
		resizeint, err := strconv.Atoi(resizeParam)
		if err != nil {
			serveError(c, w, err)
			return
		}
		reader := blobstore.NewReader(c, blobkey)
		buf, errr := resize.ResizeImage(reader, resizeint)
		if errr != nil {
			serveError(c, w, errr)
			return
		}
		w.Header().Set("Content-Type", "image/png")
		_, errrr := w.Write(buf)
		if errrr != nil {
			serveError(c, w, errr)
		}
		return
	}

	blobstore.Send(w, appengine.BlobKey(blobkey))
}

func setResourceHeaders(w http.ResponseWriter, r *http.Request, mediaObject *MediaObject) (statusNotModified bool) {
	lastModifiedString := mediaObject.Creation.Format(time.RFC1123)
	expires := mediaObject.Creation.AddDate(0, 0, 30 /*days*/).Format(time.RFC1123)
	w.Header().Set("Last-Modified", lastModifiedString)
	w.Header().Set("Expires", expires)
	w.Header().Set("Cache-Control", "private")
	w.Header().Set("Content-Type", mediaObject.ContentType)

	if ims := r.Header.Get("If-Modified-Since"); ims != "" {
		if ims == lastModifiedString {
			w.WriteHeader(http.StatusNotModified)
			return true
		}
		modSince, err := time.Parse(time.RFC1123, ims)
		if err != nil {
			return false
		}
		if modSince.After(mediaObject.Creation) {
			w.Header().Del("Content-Type")
			w.WriteHeader(http.StatusNotModified)
			return true
		}
	}
	return false
}

func mungeResourceUrl(path string) (int64, error) {
	pathelements := strings.Split(path, "/")
	return strconv.ParseInt(pathelements[2], 10, 64)
}

// GetUserInfo returns the UserInfo object for the currently logged-in user.
// both return values are nil if no user is logged in.
func GetUserInfo(c appengine.Context) (*datastore.Key, *UserInfo) {
	user := user.Current(c)
	if user == nil {
		return nil, nil
	}
	auth_email := user.String()
	key, ui, err := GetOrInsert(c, auth_email)
	if err != nil {
		c.Errorf("getOrInsert: {%v}", err)
		return nil, nil
	}
	return key, &ui
}

func handleRobots(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "User-agent: *\nDisallow: /\n")
}
