# Nometicland

Nometicland stores scans of documents.

It is a rewrite of Brad Fitzpatrick's [Scanning Cabinet][1] in Go.

There is no reason to rewrite Scanning Cabinet; this is most
certainly not me saying "Ho! I can do better than that!". This
project was, essentially, an exercise — a pleasurable expenditure
of some time during the Christmas break. Also, certainly, because
I wanted some document storage on Google App Engine.


# Server

The documentation in Mr Fitzpatrick's project describes how to
get the code running on Google App Engine. I shall, essentially,
repeat what he says here:

* Go to `http://appspot.com` to make an AppId and then edit the
  first line of `nometicland/appengine/app.yaml` with your AppId
* Run `appcfg.py update appengine` to upload to Google App Engine
* Go to `https://<your-AppId>.appspot.com/` and login 
* Having logged in, there is now a UserInfo entity in the datastore
  which you can edit in the datastore viewer of the dashboard at
  `http://appspot.com`. You want to change the UploadPassword to 
  something — anything — but preferably not your Google password

# Tools

## nometic

This is the program that you use on your client to upload files
to the server.

It needs to be configured to be able to upload to your server:

* Rename the file `nometic.gcfg.example` to .nometic.gcfg in
  your home directory or in the directory where the tool will
  be run.
* Edit the config file so that Email matches the email address
  you log in to the server with and that Password matches the
  UploadPassword you entered in the datastore viewer a few
  paragraphs ago. Enter your AppID in the UploadUrl field.
* The 'Development' section of the config file is a second copy
  of the configuration that is used when nometic is called with
  the `--dev` flag
* Compile this tool by running `go build nometic` in the tools
  directory. You may need to bash out a
  `go get code.google.com/p/gcfg` beforehand — you may not.
* Call it as `nometic [--dev] file1 [file2...]`

## nmtcpile

This I wrote for my own use because my scanner does not scan
both sides of the paper. I have to stack a wad of documents
in the top and scan them, and then grab the wad from the out
tray, turn them over and then run these back sides through
the scanner. This tool munges the file numbers so they are
in order with the back of each piece of paper sorted so it is
immediately after the front of that piece of paper. Then it
uploads them all to the server — configured with the same
mechanism as `nometic`. I don't anticipate that anybody else
will use it.



[1]: https://github.com/bradfitz/scanningcabinet
